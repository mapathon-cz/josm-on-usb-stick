JOSM on USB stick
=================

This is [JOSM][] version runnable from a flash disk under Windows. See [USB
stick][].

Download [josm-on-usb-stick.zip][].

[JOSM]: https://josm.openstreetmap.de/
[USB stick]: https://josm.openstreetmap.de/wiki/USB_Stick
[josm-on-usb-stick.zip]: https://mapathon-cz.gitlab.io/josm-on-usb-stick/josm-on-usb-stick.zip


License
-------

The project is published under [MIT License][1]. The OpenJDK, that is
downloaded and published with GitLab CI/CD, is licensed under [GNU General
Public License, version 2, with the Classpath Exception][2].

[1]: ./LICENSE
[2]: https://openjdk.java.net/legal/gplv2+ce.html


Pipeline failed!
================

Because it's designed to do so. There is scheduled GitLab CI/CD run each day at
04:00. If there is no new JOSM version, the pipeline fails and GitLab pages are
therefore not updated. Due to this setup it's rare that pipeline succeeds.

Pipeline suceeds whenever there is a new JOSM version and GitLab pages have to
be rebuilt.
