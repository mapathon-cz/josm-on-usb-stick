#!/bin/sh
josm_version()
{
        wget https://josm.openstreetmap.de/josm-tested.jar
        jar -xf josm-tested.jar
        grep Revision REVISION | cut -d' ' -f2 > josm_revision
}
download_java()
{
        wget https://github.com/AdoptOpenJDK/openjdk13-binaries/releases/download/jdk-13.0.2%2B8/OpenJDK13U-jre_x86-32_windows_hotspot_13.0.2_8.msi
        wget https://github.com/AdoptOpenJDK/openjdk13-binaries/releases/download/jdk-13.0.2%2B8/OpenJDK13U-jre_x64_windows_hotspot_13.0.2_8.msi
        wget https://download.java.net/java/GA/jdk13.0.2/d4173c853231432d94f001e99d882ca7/8/GPL/openjdk-13.0.2_windows-x64_bin.zip
        unzip openjdk-13.0.2_windows-x64_bin.zip
}
download_revision()
{
        touch revision
        wget -O revision https://mapathon-cz.gitlab.io/josm-on-usb-stick/revision
}

jv=$(josm_version && cat josm_revision)
cv=$(download_revision && cat revision)

if [ x"$jv" = x"$cv" ]
then
    echo "Fail. The versions are the same, so no need to generate pages."
    exit 1
fi

mkdir josm-on-usb-stick
mv josm-tested.jar josm-on-usb-stick/
mv runjosm.bat josm-on-usb-stick/

download_java
mv jdk-13.0.2/ josm-on-usb-stick/
mv OpenJDK13U-jre_x86-32_windows_hotspot_13.0.2_8.msi josm-on-usb-stick/
mv OpenJDK13U-jre_x64_windows_hotspot_13.0.2_8.msi josm-on-usb-stick/

zip -9 -r josm-on-usb-stick.zip josm-on-usb-stick

mkdir public
mv josm-on-usb-stick.zip public/
mv josm_revision public/revision
